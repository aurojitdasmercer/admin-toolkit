package listeners;





import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {

	
	//secretKey and secretValue can be random string but it should match with decryption part.
	

	public static String getEncryptedString(String password, String secretKey,String secretValue)
	{
		try
		{
			byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), secretValue.getBytes(), 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);
			return Base64.getEncoder().encodeToString(cipher.doFinal(password.getBytes("UTF-8")));
		}
		catch (Exception e)
		{
			System.out.println("Error while encrypting: " + e.toString());
		}
		return null;
	}




	public static void main(String[] args) {
		String p[]= {"Mercer@123","jkl@123","A12345","Password@123"};
		//String password = "Mercer@123";  //enter your password here
		for(String pp:p)
		{
		String encryptedString = getEncryptedString(pp, "TXN@$^*!","ISK*=-^"); 
		System.out.println("password --Encrypted Password "+pp+"--is--"+encryptedString);     
		}
		//System.out.println("your password" + password); 				// Needs to be removed
		
		
	}
}
