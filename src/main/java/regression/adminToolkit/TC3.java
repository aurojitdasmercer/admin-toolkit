package regression.adminToolkit;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.AdminToolkit.FileOperations;
import pages.AdminToolkit.HomePage;
import pages.AdminToolkit.SearchPage;
import utilities.InitTests;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;
import static verify.SoftAssertions.verifyElementIsNotPresent;


public class TC3 extends InitTests {
	TC3  verifySiteIsUp;
	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	
	public TC3(String appName) {
		super(appName);
	}
	
	@Test(enabled=true, priority= 1, retryAnalyzer = RetryAnalyzer.class)
	public void verifyQM() throws Exception	{		
			
		try {
			verifySiteIsUp=	new TC3("adminToolkit");	
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Client And HomePage EP verification");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			
			HomePage home = new HomePage(driver);
			FileOperations file = new FileOperations();
			file.readData();
			waitForElementToDisplay(home.clientListHeader);
			verifyElementContains(home.clientListHeader, "Client List", " , Client List Header Matched", test);			
			setInput(home.filterSearchBox, "smlmkt");
			waitForElementToDisplay(home.clientName);
			verifyElementContains(home.clientName, "Mercer Marketplace Enterprise", " , Client Name Header Matched", test);
			verifyElementContains(home.clientCode, "SMLMKT", " , Client OneCode Name Header Matched", test);
			clickElement(home.clientName);
			
			waitForElementToDisplay(home.webConfiguration);
			verifyElementContains(home.webConfiguration, "Web Configuration", " , Web Configuration Header Matched", test);
			verifyElementContains(home.WebConfigDesc, "The following are Evaluations Points for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(home.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			verifyElementContains(home.lineOFBusiness, "HB,PORTAL", " ,  Line of Business Matched", test);
			verifyElementContains(home.clientOneCOde, "SMLMKT", " ,  Client code Matched", test);
			verifyElementContains(home.visibilityDate, home.date, " , Visibility Date Matched", test);
			
			home.waitForPageload();
			
			// 1st EP
			test = reports.createTest("Verify Advanced Search");
			SearchPage search = new SearchPage(driver);
			verifyElementContains(search.advancedSearchButton, "[ Show Advanced Search ]", ", Advanced Search Button is Discplayed", test);
			clickElement(search.advancedSearchButton);
			waitForElementToDisplay(search.advancedSearchHeader);
			verifyElementContains(search.advancedSearchHeader, "Advanced Search:", ", Advanced search header displayed", test);
			verifyElementContains(search.advSearchDesc, "Enter your search text, and then choose your search option. Use the filter textbox to further refine your search results.", ", Advanced Search Description matched", test);
			clickElement(search.closeAdvSearch);
			//verifyElementIsNotPresent(driver, search.advancedSearchHeader, test, "Advanced Search Header", ", Advanced search header not displayed");
			
			Select select = new Select(search.serviceContainerDD);
			select.selectByVisibleText("Document Upload");
			clickElement(search.searchButton);
			
			Thread.sleep(5000);
			
		/*	for(WebElement e:search.serviceContailerResults) {
				verifyElementContains(e, "Document Upload", ", Service Cointainer matched in result", test);
			}
			*/
			waitForElementToDisplay(By.xpath("//td[@data-bind='text: level1']"));
			System.out.println("sbhdgshgdk"+search.serviceContailerResults.size());
			
			for(int i=0;i<7;i++) {
				verifyElementContains(search.serviceContailerResults.get(i), "Document Upload", ", Service Cointainer matched in result", test);
				
			}
			
		}
		
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
			driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.quit();

		}
	}
}
	
	