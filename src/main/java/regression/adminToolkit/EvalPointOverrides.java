package regression.adminToolkit;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import static utilities.DatabaseUtils.getResultSet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.AdminToolkit.FileOperations;
import pages.AdminToolkit.HomePage;
import pages.AdminToolkit.ConfigPortalPage;
import pages.AdminToolkit.SearchPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;
import static verify.SoftAssertions.verifyElementIsNotPresent;

import java.sql.Connection;
import java.sql.ResultSet;


public class EvalPointOverrides extends InitTests {
	EvalPointOverrides  verifySiteIsUp;
	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	
	public EvalPointOverrides(String appName) {
		super(appName);
	}
	
	@Test(enabled=false, priority= 1, retryAnalyzer = RetryAnalyzer.class)
	public void verifyQM() throws Exception	{		
			
		try {
			verifySiteIsUp=	new EvalPointOverrides("adminToolkit");	
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify EvalPoint Override");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			
			HomePage home = new HomePage(driver);
			FileOperations file = new FileOperations();
			file.readData();
			waitForElementToDisplay(home.clientListHeader);
			verifyElementContains(home.clientListHeader, "Client List", " , Client List Header Matched", test);			
			setInput(home.filterSearchBox, "smlmkt");
			waitForElementToDisplay(home.clientName);
			verifyElementContains(home.clientName, "Mercer Marketplace Enterprise", " , Client Name Header Matched", test);
			verifyElementContains(home.clientCode, "SMLMKT", " , Client OneCode Name Header Matched", test);
			clickElement(home.clientName);
			
			waitForElementToDisplay(home.webConfiguration);
			verifyElementContains(home.webConfiguration, "Web Configuration", " , Web Configuration Header Matched", test);
			verifyElementContains(home.WebConfigDesc, "The following are Evaluations Points for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(home.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			verifyElementContains(home.lineOFBusiness, "HB,PORTAL", " ,  Line of Business Matched", test);
			verifyElementContains(home.clientOneCOde, "SMLMKT", " ,  Client code Matched", test);
			//verifyElementContains(home.visibilityDate, home.date, " , Visibility Date Matched", test);
			
			home.waitForPageload();
			
			//test = reports.createTest("Verify EP Enable New Hire Self Service Mode ");
			setInput(home.epSearchBox, file.epOverideName);		
			waitForElementToDisplay(home.firstEpName);
			clickElement(home.firstEpName);
			home.waitForPageload();	
			waitForElementToDisplay(home.epHeaderNAME);
			Thread.sleep(3000);
			verifyElementContains(home.epHeaderNAME, file.epOverideName, ", EP Header Matched", test);
			home.cloneEp(file.companyId);
			verifyElementContains(home.genereateOverrideHeader, "Generate Evalution Point Override", ", Genereate Override Header Matched", test);
			verifyElementContains(home.genereateOverrideDesc, "Please select the override options for this Evaluation Point", ", Genereate Override Desc Matched", test);
			
			if(file.overrideStatus.equalsIgnoreCase("yes")||file.overrideStatus.equalsIgnoreCase("y")) {
				clickElement(home.submitButton);
				waitForElementToDisplay(home.delOverrideErrorMsg);
				verifyElementContains(home.delOverrideErrorMsg, "Error", ", Override already Exists", test);
				
			}else {
				
				clickElement(home.submitButton);
				waitForElementToDisplay(home.delOverrideSuccessMsg);
				verifyElementContains(home.delOverrideSuccessMsg, "Confirmation", ", Override already Exists", test);
				
			}
			
			home.waitForPageload();	
			driver.navigate().refresh();
			
			//Verify Override success
			
			
			waitForElementToDisplay(home.clientListHeader);
			verifyElementContains(home.clientListHeader, "Client List", " , Client List Header Matched", test);			
			setInput(home.filterSearchBox, "smlmkt");
			waitForElementToDisplay(home.clientName);
			verifyElementContains(home.clientName, "Mercer Marketplace Enterprise", " , Client Name Header Matched", test);
			verifyElementContains(home.clientCode, "SMLMKT", " , Client OneCode Name Header Matched", test);
			clickElement(home.clientName);
			
			waitForElementToDisplay(home.webConfiguration);
			verifyElementContains(home.webConfiguration, "Web Configuration", " , Web Configuration Header Matched", test);
			verifyElementContains(home.WebConfigDesc, "The following are Evaluations Points for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(home.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			verifyElementContains(home.lineOFBusiness, "HB,PORTAL", " ,  Line of Business Matched", test);
			verifyElementContains(home.clientOneCOde, "SMLMKT", " ,  Client code Matched", test);
			//verifyElementContains(home.visibilityDate, home.date, " , Visibility Date Matched", test);
			
			home.waitForPageload();
			
			
			setInput(home.epSearchBox, file.epOverideNameverification);		
			waitForElementToDisplay(home.firstEpName);
			clickElement(home.firstEpName);
			home.waitForPageload();	
			waitForElementToDisplay(home.epHeaderNAME);
			Thread.sleep(3000);
			verifyElementContains(home.epHeaderNAME, file.epOverideNameverification, ", EP Header Matched", test);
			
			clickElement(home.genereateOverride);
			waitForElementToDisplay(home.overrideSuccessMsg);
			verifyElementContains(home.overrideSuccessMsg, "Override for Evaluation Point generated successfully.", " ,Override Suces", test);
			waitForElementToDisplay(home.authoringOverrideStatus);
			verifyElementContains(home.authoringOverrideStatus, "in sync", ", Authoring Ovveride IN sync", test);
			
			home.copyRule(file.expression);
			waitForElementToDisplay(home.vadlidExpMsg);
			verifyElementContains(home.vadlidExpMsg, "The expression you have written is valid.", ", Expression validated", test);
			try {
				
				String value = home.ruleLabel.getAttribute("value");
				home.ruleLabel.clear();			
				home.ruleLabel.sendKeys("Automation - "+value);
		
			} catch (Exception e) {
			
		
			}
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			
			
			Connection ccaDB= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
			  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa15,cn=OracleContext,dc=world","Z_MM_CC_A","MM_CC_A_zqaFri2019");
			
			String query= "select * from ee_override where company_id='56443' order by updated_date desc";
			ResultSet query1= getResultSet(ccaDB, query);
			String eval_point_idA ="";
			
			
			if(query1.next()) {
				eval_point_idA =  query1.getString(2);
				System.out.println(eval_point_idA);
			}
			
			//promoteEp
			
			test = reports.createTest("Promote EvalPoint");
			
			driver.get(props.getProperty("cp_baseurl"));
			
			ConfigPortalPage cp = new ConfigPortalPage(driver);
			cp.login(props.getProperty("cp_username"), props.getProperty("cp_password"));	
			waitForElementToDisplay(cp.homeIcon);
			driver.get("https://consultant-qaf.mercermarketplace365plus.com/#/activeClients/individualClient/56443/2021");
			cp.waitForLoadingPage();
			clickElement(cp.adminTab);
			cp.waitForLoadingPage();
			waitForElementToDisplay(cp.promoteEvalPoint);
			clickElement(cp.promoteEvalPoint);
			waitForElementToDisplay(cp.promoteEvalPointHeader);
			verifyElementContains(cp.promoteEvalPointHeader, "Promotion Evaluation Engine Overrides", ", Promote EvalPoint", test);
			setInput(cp.promoteEvalPointHInput, eval_point_idA);
			clickElement(cp.promoteButton);
			waitForElementToDisplay(cp.promoteSucessMsg);
			verifyElementContains(cp.promoteSucessMsg,"All Employee Website Eval Engine Overrides for SpaceX have been promoted to Delivery successfully!", ", Ep Promote Success", test);
			
			
			// VerifyProimotion
			driver.get(BASEURL);
			waitForElementToDisplay(home.clientListHeader);
			verifyElementContains(home.clientListHeader, "Client List", " , Client List Header Matched", test);			
			setInput(home.filterSearchBox, "smlmkt");
			waitForElementToDisplay(home.clientName);
			verifyElementContains(home.clientName, "Mercer Marketplace Enterprise", " , Client Name Header Matched", test);
			verifyElementContains(home.clientCode, "SMLMKT", " , Client OneCode Name Header Matched", test);
			clickElement(home.clientName);
			
			waitForElementToDisplay(home.webConfiguration);
			verifyElementContains(home.webConfiguration, "Web Configuration", " , Web Configuration Header Matched", test);
			verifyElementContains(home.WebConfigDesc, "The following are Evaluations Points for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(home.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			verifyElementContains(home.lineOFBusiness, "HB,PORTAL", " ,  Line of Business Matched", test);
			verifyElementContains(home.clientOneCOde, "SMLMKT", " ,  Client code Matched", test);
			verifyElementContains(home.visibilityDate, home.date, " , Visibility Date Matched", test);
			
			home.waitForPageload();
			setInput(home.epSearchBox, file.epOverideNameverification);		
			waitForElementToDisplay(home.firstEpName);
			clickElement(home.firstEpName);
			home.waitForPageload();	
			waitForElementToDisplay(home.epHeaderNAME);
			Thread.sleep(3000);
			verifyElementContains(home.epHeaderNAME, file.epOverideNameverification, ", EP Header Matched", test);
			waitForElementToDisplay(home.delOverrideDel);
			verifyElementContains(home.authoringOverrideStatus, "in sync", ", Authoring Status matched", test);
			verifyElementContains(home.deliveryOverrideStatus,  "in sync", ", Delivery Status matched", test);
			

			Connection ccdDB= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
			  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_CC_D","MM_CC_D_zqaFri2019");
			
			 query= "select * from ee_override where company_id='56443' order by updated_date desc";
			 query1= getResultSet(ccdDB, query);
			 String eval_point_idD ="";
			
			
			if(query1.next()) {
				eval_point_idD =  query1.getString(2);
				System.out.println(eval_point_idD);
			}
			
			if(eval_point_idD.equalsIgnoreCase(eval_point_idA)) {
				
				test.log(Status.PASS, "Override success in Delivery");
			}else {
				test.log(Status.FAIL, "Override FAIL in Delivery");
			}
			
			
			
			/*clickElement(home.delOverrideAuth);
			waitForElementToDisplay(home.delOverrideAuthSuccess);
			verifyElementContains(home.delOverrideAuthSuccess, "Override for Evaluation Point deleted successfully (Authoring).", ", Override Successfully deleted from Authoring", test);
			clickElement(home.delOverrideDel);
			waitForElementToDisplay(home.delOverrideDelSuccess);
			verifyElementContains(home.delOverrideDelSuccess, "Override for Evaluation Point deleted successfully (Authoring).", ", Override deleted succesfully from Delhivery", test);
			
			*/
			
			
			/*Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa15,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			 
			
		
			String query = "";
			ResultSet query1= getResultSet(mycon, query);*/
			
		}
		
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
			driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.quit();

		}
	}
	@Test(enabled=true, priority= 2, retryAnalyzer = RetryAnalyzer.class)
	public void verifyUI() throws Exception	{		
		
		try {
			verifySiteIsUp=	new EvalPointOverrides("qa_authoring");	
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify EvalPoint Override");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			
			HomePage home = new HomePage(driver);
			FileOperations file = new FileOperations();
			file.readData();
			
			ConfigPortalPage cp = new ConfigPortalPage(driver);
			cp.login(USERNAME, PASSWORD);	
			waitForElementToDisplay(cp.homeIconMM365);
			waitForElementToDisplay(cp.JustForYouHeader);
			verifyElementContains(cp.JustForYouHeader, "Just For You", ", Just For you Header", test);
			
			
			/*clickElement(home.delOverrideAuth);
			waitForElementToDisplay(home.delOverrideAuthSuccess);
			verifyElementContains(home.delOverrideAuthSuccess, "Override for Evaluation Point deleted successfully (Authoring).", ", Override Successfully deleted from Authoring", test);
			clickElement(home.delOverrideDel);
			waitForElementToDisplay(home.delOverrideDelSuccess);
			verifyElementContains(home.delOverrideDelSuccess, "Override for Evaluation Point deleted successfully (Authoring).", ", Override deleted succesfully from Delhivery", test);
			
			*/
			
			
			/*Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa15,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			 
			
		
			String query = "";
			ResultSet query1= getResultSet(mycon, query);*/
			
		}
		
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
			driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.quit();

		}
	}
}
	
	