package regression.adminToolkit;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

/*import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;*/
import driverfactory.Driver;
import pages.AdminToolkit.BestMatchPage;
import pages.AdminToolkit.BpoCarrierDescPage;
import pages.AdminToolkit.ClientSettingsPage;
import pages.AdminToolkit.FileOperations;
import pages.AdminToolkit.HomePage;
import utilities.InitTests;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;
import static verify.SoftAssertions.verifyEquals;
import static verify.SoftAssertions.verifyElementIsPresent;


public class TC1 extends InitTests {
	TC1  verifySiteIsUp;
	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	
	public TC1(String appName) {
		super(appName);
	}
	
	@Test(enabled=true, priority= 1, retryAnalyzer = RetryAnalyzer.class)
	public void verifyQM() throws Exception	{		
			
		try {
			verifySiteIsUp=	new TC1("adminToolkit");	
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("verify clientList And HomePage" );
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			
			HomePage home = new HomePage(driver);
			FileOperations file = new FileOperations();
			file.readData();
			waitForElementToDisplay(home.clientListHeader);
			verifyElementContains(home.clientListHeader, "Client List", " , Client List Header Matched", test);			
			setInput(home.filterSearchBox, "smlmkt");
			waitForElementToDisplay(home.clientName);
			verifyElementContains(home.clientName, "Mercer Marketplace Enterprise", " , Client Name Header Matched", test);
			verifyElementContains(home.clientCode, "SMLMKT", " , Client OneCode Name Header Matched", test);
			clickElement(home.clientName);
			
			waitForElementToDisplay(home.webConfiguration);
			verifyElementContains(home.webConfiguration, "Web Configuration", " , Web Configuration Header Matched", test);
			verifyElementContains(home.WebConfigDesc, "The following are Evaluations Points for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(home.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			verifyElementContains(home.lineOFBusiness, "HB,PORTAL", " ,  Line of Business Matched", test);
			verifyElementContains(home.clientOneCOde, "SMLMKT", " ,  Client code Matched", test);
			verifyElementContains(home.visibilityDate, home.date, " , Visibility Date Matched", test);
			clickElement(home.bpoCarrierDesctab);
			
			test = reports.createTest("verify CarrierDesc" );
			BpoCarrierDescPage bpo = new BpoCarrierDescPage(driver);
			waitForElementToDisplay(bpo.WebConfigBPODesc);
			verifyElementContains(bpo.webConfigurationBPO, "Web Configuration - BPO / Carrier Descriptions (BW)", " , Web Configuration Header Matched", test);
			verifyElementContains(bpo.WebConfigBPODesc, "Select the pages below to update the BPO Descriptions and Carrier Descriptions used for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(bpo.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			
			//CarrierDesc
			
			clickElement(bpo.carrierDescriptions);
			waitForElementToDisplay(bpo.webConfigurationCarrierDesc);
			verifyElementContains(bpo.webConfigurationCarrierDesc, "Web Configuration - Carrier Descriptions", "Carrier desc text matched", test);
			
			bpo.searchAndEditCarrier("AETNA_0");
			waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated Carrier Descriptions.", " Save success", test);
			if(bpo.saveCarrier.isEnabled()) {
				test.log(Status.PASS, "Save is enabled");
			}else {
				test.log(Status.FAIL, "Save is Disabled");
			}
			
			if(bpo.cancelCarrier.isEnabled()) {
				test.log(Status.PASS, "Cancel is enabled");
			}else {
				test.log(Status.FAIL, "Cancel is Disabled");
			}
			
			if(bpo.saveAndContinue.isEnabled()) {
				test.log(Status.PASS, "SaveandContinue is enabled");
			}else {
				test.log(Status.FAIL, "SaveandContinue is Disabled");
			}
			
			bpo.EditCarrierRename();
			waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated ", " Save success", test);	
			
			//Benits Description
			test = reports.createTest("verify BenifitsDesc" );
			clickElement(home.bpoCarrierDesctab);
			waitForElementToDisplay(bpo.WebConfigBPODesc);
			clickElement(bpo.benifitDescriptions);
			waitForElementToDisplay(bpo.webConfigurationBenifitsDesc);
			verifyElementContains(bpo.webConfigurationBenifitsDesc, "Web Configuration - Benefit Descriptions", "Benifits Header Matched", test);
			
			bpo.searchAndEditCarrier("ACCIDENT");
			waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated ", " Save success", test);
			if(bpo.saveCarrier.isEnabled()) {
				test.log(Status.PASS, "Save is enabled");
			}else {
				test.log(Status.FAIL, "Save is Disabled");
			}
			
			if(bpo.cancelCarrier.isEnabled()) {
				test.log(Status.PASS, "Cancel is enabled");
			}else {
				test.log(Status.FAIL, "Cancel is Disabled");
			}
			
			if(bpo.saveAndContinue.isEnabled()) {
				test.log(Status.PASS, "SaveandContinue is enabled");
			}else {
				test.log(Status.FAIL, "SaveandContinue is Disabled");
			}
			
			bpo.EditCarrierRename();
			/*waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated ", " Save success", test);	*/
			
			
			//Plan Desc
			test = reports.createTest("verify PlanDesc" );
			clickElement(home.bpoCarrierDesctab);
			waitForElementToDisplay(bpo.WebConfigBPODesc);
			clickElement(bpo.PlanDescriptions);
			waitForElementToDisplay(bpo.webConfigurationPlanDesc);
			verifyElementContains(bpo.webConfigurationPlanDesc, "Web Configuration - Plan Descriptions", "Plan Desc header Matched", test);
					
			bpo.searchAndEditCarrierPlan("COVN001E01");
			waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated ", " Save success", test);
			if(bpo.saveCarrier.isEnabled()) {
				test.log(Status.PASS, "Save is enabled");
			}else {
				test.log(Status.FAIL, "Save is Disabled");
			}
			
			if(bpo.cancelCarrier.isEnabled()) {
				test.log(Status.PASS, "Cancel is enabled");
			}else {
				test.log(Status.FAIL, "Cancel is Disabled");
			}
			
			if(bpo.saveAndContinue.isEnabled()) {
				test.log(Status.PASS, "SaveandContinue is enabled");
			}else {
				test.log(Status.FAIL, "SaveandContinue is Disabled");
			}
			
			bpo.EditCarrierRenamePaln();
			waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			//verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated ", " Save success", test);	
			
			//Optin Desc
			test = reports.createTest("verify OptionDesc" );
			home.waitForPageload();
			clickElement(home.bpoCarrierDesctab);	
			waitForElementToDisplay(bpo.WebConfigBPODesc);
			clickElement(bpo.optionsDescription);
			waitForElementToDisplay(bpo.webConfigurationoptionDesc);
			verifyElementContains(bpo.webConfigurationoptionDesc, "Web Configuration - Option Descriptions", "Option Desc header text matched", test);
			
			/*bpo.searchAndEditOption("COVN001E01");
			home.waitForPageload();
			waitForElementToDisplay(bpo.saveCarrierSuccesmsg);
			verifyElementContains(bpo.saveCarrierSuccesmsg, "You have successfully updated ", " Save success", test);*/
			if(bpo.saveCarrier.isEnabled()) {
				test.log(Status.PASS, "Save is enabled");
			}else {
				test.log(Status.FAIL, "Save is Disabled");
			}
			
			if(bpo.cancelCarrier.isEnabled()) {
				test.log(Status.PASS, "Cancel is enabled");
			}else {
				test.log(Status.FAIL, "Cancel is Disabled");
			}
			
			if(bpo.saveAndContinue.isEnabled()) {
				test.log(Status.PASS, "SaveandContinue is enabled");
			}else {
				test.log(Status.FAIL, "SaveandContinue is Disabled");
			}
			
			//Best match Tab
				
			test = reports.createTest("verify BestMatch Tab" );
			clickElement(home.bestMatchTab);
			BestMatchPage bestM = new BestMatchPage(driver);
			waitForElementToDisplay(bestM.bestMatchHeader);
			verifyElementContains(bestM.bestMatchHeader, "Web Configuration - Best Match", ", Best Match Header Matched", test);
			verifyElementContains(bestM.bestMatchDesc, "This page allows for the updating of Actuarial Value and EEONLY Deductible for Best Match for SMLMKT. This feature is currently only available for Small Market.", ", Best Match Desc Matched", test);
			verifyElementIsPresent(bestM.saveButton, test, "Save Button");
			verifyElementIsPresent(bestM.cancelButton, test, "Cancel Button");
			
			//Verify Visibility Date
			test = reports.createTest("verify Visibility Date" );
			clickElement(home.clientSettingsTtab);			
			ClientSettingsPage client = new ClientSettingsPage(driver);
			waitForElementToDisplay(By.xpath("//label[@class='col-14']"));
			clickElement(client.visibilityDateTab);
			waitForElementToDisplay(client.enabledRadioButton);
			verifyElementContains(client.visibilityDateHeader, "Visibility Date", ", Visibility date Header Matched", test);
			verifyElementIsPresent(client.visibilityDateSave, test, "Save Button");
			verifyElementIsPresent(client.visibilityDateCancel, test, "Cancel Button");
			clickElement(client.enabledRadioButton);
			clickElement(client.datePicker);
			waitForElementToDisplay(client.visibilityDateInput);
			clickElement(client.visibilityDateCancel);
			
			//clickElement(client.visibilityDateCancel);
			waitForElementToDisplay(client.discardMsg);
			verifyElementContains(client.discardMsg, "Your changes have not been saved. Are you sure you want to cancel?", ", Discrd Msg matched", test);
			clickElement(client.discardButton);
			
			//Hb Client Settings tab verification
			
			
			
			test = reports.createTest("verify Hb Client Settings tab" );
			 clickElement(client.hbClientSettingsTab);
			 waitForElementToDisplay(By.xpath("//label[@class='col-14']"));
			waitForElementToDisplay(client.ClientSettingsHeader);
			verifyElementContains(client.ClientSettingsHeader, "Web Configuration - Client Settings", " Client Settings header", test);
			waitForElementToDisplay(client.hbSettingsHeader);
			
			
			int i = 0;
			for(WebElement e : client.hbSettingsName) {
				verifyEquals(e.getText(), file.HbSettingsName.get(i), test);
				i++;
			}
			
			i=0;
					
			for(WebElement e : client.hbSettingsOptions) {
				verifyEquals(file.HbSettingsOptions.get(i), e.getText(), test);
				i++;
			}
			
			//PortalTab Verification
			test = reports.createTest("verify PortalTab" );
			clickElement(client.portalTab);
			waitForElementToDisplay(By.xpath("//label[@class='col-14']"));
			i=0;
			for(WebElement e : client.portalSettingsName) {
				verifyEquals(e.getText(), file.portalSettingName.get(i), test);
				i++;
			}
			
			i=0;
					
			for(WebElement e : client.portalSettingstextbox) {
				verifyEquals(file.portalSettingsOptions.get(i), e.getAttribute("value"), test);
				if(e.getAttribute("value").equalsIgnoreCase(file.portalSettingsOptions.get(i))) {
					
				}else {
					System.out.println("value didnt match...changing value");
					setInput(e, file.portalSettingsOptions.get(i));
				}
				
				i++;
			}
					
			
			
		}
		
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
			driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.quit();

		}
	}
}
	
	