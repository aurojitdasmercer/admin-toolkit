package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class DataDriven {

public static ArrayList<String> readExcel(String testcaseName) throws IOException{	
		ArrayList<String> a=new ArrayList<String>();
		//getData("Employee Only");
		String filePath = "./src/main/resources/testdata/MBCLE_TestData.xlsx";
		FileInputStream fis=new FileInputStream(filePath);
		XSSFWorkbook workbook=new XSSFWorkbook(fis); //setting a new workbook
		XSSFSheet sheet = workbook.getSheetAt(0); //setting up the sheetname
		for (int i = 1; i <= sheet.getLastRowNum(); i++) { //iterating till the last row
			if (sheet.getRow(i).getCell(0).getStringCellValue().equalsIgnoreCase(testcaseName)) //checking which row matches the test case name
			{
				for(int j =0; j<sheet.getRow(0).getLastCellNum();j++) //iterating to each non empty cells in the matched row
				{
					a.add(sheet.getRow(i).getCell(j).getStringCellValue()); // getting the cell values
				}
			}
		}
		workbook.close();
		return a;
	}
	/*
	 * public static void main(String[] args) throws IOException {
	 * System.out.println(readExcel("Employee + Spouse")); }
	 * 
	 */
}