/**
 * 
 */
package utilities;

import java.util.List;

public class ListUtil
{
	
	public static boolean checkIfListContainsOtherList(List<String> list1 , List<String> list2)
	{
		int count=0;
		for(int i =0;i<list2.size();i++)
		{
			if(list1.contains(list2.get(i).trim()))
				count++;
		}
		if (count==list2.size())
			return true;
		else
			return false;
	}

}
