package utilities;

public class ExcelInput {

	private static String Coverage_OtherBenefits, MedicalPrName,  Med_Contri,Dependents,
	DentalPrName, Den_Contri, VisionPrName, Vision_Contri,MedicalParam,DentalParam,VisionParam,
	SpAcc_Contri, SupHospital,SupAccidentalInsuarance,SupCriticalIllness,SupCancerSelect,ManageBeneficiary,
	ProtectionPrName,SpAccHSA,SpAccHCFSA,SpAccLPFSA,SpAccDCSA,LifeBasicLife,LifeVoluntaryEmployeeInsurance,LifeVoluntarySpouseInsurance
	,LifeVoluntaryChildInsurance,LifeUniversalEmployeeLifeInsurance,LifeUniversaSpouseLifeInsurance,LifeUniversalChildLifeInsurance,LifeVoluntaryAD
	,LifeContri,DisabilityShortTerm,DisabilityLongTerm,DisabilitySupplementalShortTerm,DisabilitySupplementalLongTerm ,ABCommuterParking,ABCommuterTransit,ABLegalAssistance,AB365HUB,
	ABContri,VoluntaryHomeIns,VoluntaryPetIns,VoluntaryPerLifeIns,VoluntaryStd
;

	//TestCases MedicalPrName	Med_Contri	SuplePrName DentalPrName
	//Den_Contri VisionPrName	Vision_Contri SpAccPrName
	//SpAcc_Contri	LifePrName	Life_Contri	DisabilityPrName
	//ProtectionPrName	ABPrName

	public static void setCoverage_OtherBenefits(String Coverage_OtherBenefits)
	{
		ExcelInput.Coverage_OtherBenefits = Coverage_OtherBenefits;
	}
	public static String getCoverage_OtherBenefits() {
		return Coverage_OtherBenefits;
	}	
	
	public static void setDependents(String Dependents)
	{
		ExcelInput.Dependents = Dependents;
	}
	public static String getDependents() {
		return Dependents;
	}	

	
	public static void setVisionParam(String VisionParam)
	{
		ExcelInput.VisionParam = VisionParam;
	}
	public static String getVisionParam() {
		return VisionParam;
	}
	
	public static void setMedicalParam(String MedicalParam)
	{
		ExcelInput.MedicalParam = MedicalParam;
	}
	public static String getMedicalParam() {
		return MedicalParam;
	}
	
	public static void setDentalParam(String DentalParam)
	{
		ExcelInput.DentalParam = DentalParam;
	}
	public static String getDentalParam() {
		return DentalParam;
	}
	
	public static void setMedicalPrName(String MedicalPrName)
	{
		ExcelInput.MedicalPrName = MedicalPrName;
	}
	public static String getMedicalPrName() {
		return MedicalPrName;
	}

	public static void setSupHospital(String SupHospital)
	{
		ExcelInput.SupHospital = SupHospital;
	}
	public static String getSupHospital() {
		return SupHospital;
	}
	
	public static void setSupAccidentalInsuarance(String SupAccidentalInsuarance)
	{
		ExcelInput.SupAccidentalInsuarance = SupAccidentalInsuarance;
	}
	public static String getSupAccidentalInsuarance() {
		return SupAccidentalInsuarance;
	}
	
	public static void setSupCriticalIllness(String SupCriticalIllness)
	{
		ExcelInput.SupCriticalIllness = SupCriticalIllness;
	}
	public static String getSupCriticalIllness() {
		return SupCriticalIllness;
	}
	
	public static void setSupCancerSelect(String SupCancerSelect)
	{
		ExcelInput.SupCancerSelect = SupCancerSelect;
	}
	public static String getSupCancerSelect() {
		return SupCancerSelect;
	}
	public static void setMed_Contri(String Med_Contri)
	{
		ExcelInput.Med_Contri = Med_Contri;
	}
	public static String getMed_Contri() {
		return Med_Contri;
	}
	public static void setDentalPrName(String DentalPrName)
	{
		ExcelInput.DentalPrName = DentalPrName;
	}
	public static String getDentalPrName() {
		return DentalPrName;
	}

	public static void setDen_Contri(String Den_Contri)
	{
		ExcelInput.Den_Contri = Den_Contri;
	}
	public static String getDen_Contri() {
		return Den_Contri;
	}

	public static void setVisionPrName(String VisionPrName)
	{
		ExcelInput.VisionPrName = VisionPrName;
	}
	public static String getVisionPrName() {
		return VisionPrName;
	}

	public static void setVision_Contri(String Vision_Contri)
	{
		ExcelInput.Vision_Contri = Vision_Contri;
	}
	public static String getVision_Contri() {
		return Vision_Contri;
	}

	public static void setSpAcc_Contri(String SpAcc_Contri)
	{
		ExcelInput.SpAcc_Contri = SpAcc_Contri;
	}
	public static String getSpAcc_Contri() {
		return SpAcc_Contri;
	}

	public static void setProtectionPrName(String ProtectionPrName)
	{
		ExcelInput.ProtectionPrName = ProtectionPrName;
	}
	public static String getProtectionPrName() {
		return ProtectionPrName;
	}

	public static void setSpAccHSA(String SpAccHSA)
	{
		ExcelInput.SpAccHSA = SpAccHSA;
	}
	public static String getSpAccHSA() {
		return SpAccHSA;
	}
	
	public static void setSpAccHCFSA(String SpAccHCFSA)
	{
		ExcelInput.SpAccHCFSA = SpAccHCFSA;
	}
	public static String getSpAccHCFSA() {
		return SpAccHCFSA;
	}
	public static void setSpAccLPFSA(String SpAccLPFSA)
	{
		ExcelInput.SpAccLPFSA = SpAccLPFSA;
	}
	public static String getSpAccLPFSA() {
		return SpAccLPFSA;
	}
	public static void setSpAccDCSA(String SpAccDCSA)
	{
		ExcelInput.SpAccDCSA = SpAccDCSA;
	}
	public static String getSpAccDCSA() {
		return SpAccDCSA;
	}
	public static void setLifeBasicLife(String LifeBasicLife)
	{
		ExcelInput.LifeBasicLife = LifeBasicLife;
	}
	public static String getLifeBasicLife() {
		return LifeBasicLife;
	}
	public static void setLifeVoluntaryEmployeeInsurance(String LifeVoluntaryEmployeeInsurance)
	{
		ExcelInput.LifeVoluntaryEmployeeInsurance = LifeVoluntaryEmployeeInsurance;
	}
	public static String getLifeVoluntaryEmployeeInsurance() {
		return LifeVoluntaryEmployeeInsurance;
	}
	public static void setLifeVoluntarySpouseInsurance(String LifeVoluntarySpouseInsurance)
	{
		ExcelInput.LifeVoluntarySpouseInsurance = LifeVoluntarySpouseInsurance;
	}
	public static String getLifeVoluntarySpouseInsurance() {
		return LifeVoluntarySpouseInsurance;
	}
	public static void setLifeVoluntaryChildInsurance(String LifeVoluntaryChildInsurance)
	{
		ExcelInput.LifeVoluntaryChildInsurance = LifeVoluntaryChildInsurance;
	}
	public static String getLifeVoluntaryChildInsurance() {
		return LifeVoluntaryChildInsurance;
	}
	public static void setLifeUniversalEmployeeLifeInsurance(String LifeUniversalEmployeeLifeInsurance)
	{
		ExcelInput.LifeUniversalEmployeeLifeInsurance = LifeUniversalEmployeeLifeInsurance;
	}
	public static String getLifeUniversalEmployeeLifeInsurance() {
		return LifeUniversalEmployeeLifeInsurance;
	}
	
	public static void setLifeUniversaSpouseLifeInsurance(String LifeUniversaSpouseLifeInsurance)
	{
		ExcelInput.LifeUniversaSpouseLifeInsurance = LifeUniversaSpouseLifeInsurance;
	}
	public static String getLifeUniversaSpouseLifeInsurance() {
		return LifeUniversaSpouseLifeInsurance;
	}
	
	public static void setLifeUniversalChildLifeInsurance(String LifeUniversalChildLifeInsurance)
	{
		ExcelInput.LifeUniversalChildLifeInsurance = LifeUniversalChildLifeInsurance;
	}
	public static String getLifeUniversalChildLifeInsurance() {
		return LifeUniversalChildLifeInsurance;
	}
	
	public static void setLifeVoluntaryAD(String LifeVoluntaryAD)
	{
		ExcelInput.LifeVoluntaryAD = LifeVoluntaryAD;
	}
	public static String getLifeVoluntaryAD() {
		return LifeVoluntaryAD;
	}
	
	public static void setDisabilityShortTerm(String DisabilityShortTerm)
	{
		ExcelInput.DisabilityShortTerm = DisabilityShortTerm;
	}
	public static String getDisabilityShortTerm() {
		return DisabilityShortTerm;
	}
	
	public static void setDisabilityLongTerm(String DisabilityLongTerm)
	{
		ExcelInput.DisabilityLongTerm = DisabilityLongTerm;
	}
	public static String getDisabilityLongTerm() {
		return DisabilityLongTerm;
	}

	public static void setDisabilitySupplementalShortTerm(String DisabilitySupplementalShortTerm)
	{
		ExcelInput.DisabilitySupplementalShortTerm = DisabilitySupplementalShortTerm;
	}
	public static String getDisabilitySupplementalShortTerm() {
		return DisabilitySupplementalShortTerm;
	}
	public static void setDisabilitySupplementalLongTerm(String DisabilitySupplementalLongTerm)
	{
		ExcelInput.DisabilitySupplementalLongTerm = DisabilitySupplementalLongTerm;
	}
	public static String getDisabilitySupplementalLongTerm() {
		return DisabilitySupplementalLongTerm;
	}
	public static void setABCommuterParking(String ABCommuterParking)
	{
		ExcelInput.ABCommuterParking = ABCommuterParking;
	}
	public static String getABCommuterParking() {
		return ABCommuterParking;
	}
	public static void setABCommuterTransit(String ABCommuterTransit)
	{
		ExcelInput.ABCommuterTransit = ABCommuterTransit;
	}
	public static String getABCommuterTransit() {
		return ABCommuterTransit;
	}
	public static void setABLegalAssistance(String ABLegalAssistance)
	{
		ExcelInput.ABLegalAssistance = ABLegalAssistance;
	}
	public static String getABLegalAssistance() {
		return ABLegalAssistance;
	}
	public static void setAB365HUB(String AB365HUB)
	{
		ExcelInput.AB365HUB = AB365HUB;
	}
	public static String getAB365HUB() {
		return AB365HUB;
	}
	public static void setABContri(String ABContri)
	{
		ExcelInput.ABContri = ABContri;
	}
	public static String getABContri() {
		return ABContri;
	}
	public static void setVoluntaryHomeIns(String VoluntaryHomeIns)
	{
		ExcelInput.VoluntaryHomeIns = VoluntaryHomeIns;
	}
	public static String getVoluntaryHomeIns() {
		return VoluntaryHomeIns;
	}
	public static void setVoluntaryPetIns(String VoluntaryPetIns)
	{
		ExcelInput.VoluntaryPetIns = VoluntaryPetIns;
	}
	public static String getVoluntaryPetIns() {
		return VoluntaryPetIns;
	}
	
	public static void setVoluntaryPerLifeIns(String VoluntaryPerLifeIns)
	{
		ExcelInput.VoluntaryPerLifeIns = VoluntaryPerLifeIns;
	}
	public static String getVoluntaryPerLifeIns() {
		return VoluntaryPerLifeIns;
	}
	
	public static void setVoluntaryStd(String VoluntaryStd)
	{
		ExcelInput.VoluntaryStd = VoluntaryStd;
	}
	public static String getVoluntaryStd() {
		return VoluntaryStd;
	}
	
	public static void setLifeContri(String LifeContri)
	{
		ExcelInput.LifeContri = LifeContri;
	}
	public static String getLifeContri() {
		return LifeContri;
	}
	public static void setManageBeneficiary(String ManageBeneficiary)
	{
		ExcelInput.ManageBeneficiary = ManageBeneficiary;
	}
	public static String getManageBeneficiary() {
		return ManageBeneficiary;
	}
}
