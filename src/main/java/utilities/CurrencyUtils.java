package utilities;

import java.text.DecimalFormat;

import org.openqa.selenium.WebElement;

public class CurrencyUtils {
	public static int getCurrencyInInt(WebElement currency) {
		String price=currency.getText();
		String a[]=price.split(" ");
		System.out.println("a[1]"+a[1]);
		String t[]=a[1].split("\\.");
		System.out.println("t[0]"+t[0]);
		t[0]=t[0].replace(",", "");
		System.out.println("price in convert int --"+Integer.parseInt(t[0]));
		return Integer.parseInt(t[0]);

	}
	public static double getRoundOffCurrency(double currency)
	{
		//double totalOppInUSD ;
		DecimalFormat df = new DecimalFormat("#.##");      
		return Double.valueOf(df.format(currency));
	}
}
