  package utilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

public class PDFReader {

	public static String readPDfContent(String pathOfFile) {
		String pdfFileInText = "";
		try {
			PDDocument document = PDDocument.load(new File(pathOfFile));
			document.getClass();
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper tStripper = new PDFTextStripper();
				pdfFileInText = tStripper.getText(document);
				System.out.println(pdfFileInText);

			}
			return pdfFileInText;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;

		}
	}
	
	public static void dwnldPDFToPath(String pdfLocation) throws AWTException, InterruptedException {
	       
        
	      
	    System.out.println(pdfLocation);
		delFileIfExist(pdfLocation);
		
       Robot r = new Robot();
       Thread.sleep(3000);
       r.mousePress(InputEvent.BUTTON3_MASK);
       Thread.sleep(3000);
       r.mouseRelease(InputEvent.BUTTON3_MASK);
       Thread.sleep(3000);
       r.keyPress(KeyEvent.VK_DOWN);
       Thread.sleep(3000);
       r.keyPress(KeyEvent.VK_ENTER);
       Thread.sleep(3000);
     
     // System.out.println(pdfLocation);
     
       StringSelection sel = new StringSelection(pdfLocation);
       
       // Copy to clipboard
     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
     Thread.sleep(3000);
     r.keyPress(KeyEvent.VK_CONTROL);
     Thread.sleep(3000);
     r.keyPress(KeyEvent.VK_V);
     Thread.sleep(3000);
     r.keyRelease(KeyEvent.VK_V);
     r.keyRelease(KeyEvent.VK_CONTROL);
     Thread.sleep(3000);
     r.keyPress(KeyEvent.VK_ENTER);
     Thread.sleep(3000);
     r.keyRelease(KeyEvent.VK_ENTER);
       
       
    }
	
	public static void delFileIfExist(String filePath)
	{
		 File filetoDel = new File(filePath);
	      if(filetoDel.exists())
	    	  filetoDel.delete();
	}
}
