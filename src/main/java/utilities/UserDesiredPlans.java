package utilities;

public class UserDesiredPlans {
	private String MedicalPlanOptedByUser;
	private int MedicalPlanNumOptedByUser;

	public void setMedicalPlanOptedByUser(String MedicalPlanOptedByUser) {
		this.MedicalPlanOptedByUser = MedicalPlanOptedByUser;
	}

	public String getMedicalPlanOptedByUser() {
		return MedicalPlanOptedByUser;
	}

	public void setMedicalPlanNumOptedByUser(int MedicalPlanNumOptedByUser) {
		this.MedicalPlanNumOptedByUser = MedicalPlanNumOptedByUser;
	}
	
	public int getMedicalPlanNumOptedByUser() {
		return MedicalPlanNumOptedByUser;
	}
}
