/**
 * 
 */
package utilities;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author YugandharReddyGorrep
 *
 */
import java.time.LocalDate;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
public class WindowUtilities
{
	
	/**
	 * @description:Returns the current time stamp
	 * 
	 * 
	 * @return String
	 * @throws ParseException 
	 */
	
	WebDriver driverInstance;
	
	public WebDriver switchToWindowWithURL(String url,WebDriver driver)
	{
		
		Set<String> windows=driver.getWindowHandles();
		for(String win:windows)
		{
			
		if(((driver.switchTo().window(win)).getCurrentUrl()).contains(url))
			{
				driverInstance=driver.switchTo().window(win);
				break;
			
			}
		}
		return driverInstance;
	
	}
	
	public WebDriver switchToParentPage(WebDriver driver , String winHandle)
	{
		driver.close();
		driverInstance = driver.switchTo().window(winHandle);
		return driverInstance;
	}
	
	public WebDriver switchToWindowWithTitle(String title , WebDriver driver)
	{
		Set<String> tabHandles=driver.getWindowHandles();
		String currHandle = driver.getWindowHandle();
		for(String eachHandle : tabHandles)
		{
		  //  driver.switchTo().window(eachHandle);
		    // Check Your Page Title 
			if(!eachHandle.equalsIgnoreCase(currHandle)) {
		    if(driver.switchTo().window(eachHandle).getTitle().contains(title))
		    {
		        // Report ur new tab is found with appropriate title 
		    	
		    	driverInstance= driver.switchTo().window(eachHandle);
		       
		        break;
		                   
		    }}
		}
		return driverInstance;
	}
}