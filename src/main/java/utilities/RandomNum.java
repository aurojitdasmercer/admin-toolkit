/**
 * 
 */
package utilities;
import java.text.ParseException;
/**
 * @author YugandharReddyGorrep
 *
 */

import java.util.Random;
public class RandomNum
{
	public  String getFourDigitRandom() throws ParseException
	{
		Random random = new Random(); 
		String generatePin = String.format("%04d", random.nextInt(10000));
		return generatePin;
	}
	
}
