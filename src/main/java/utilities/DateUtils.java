/**
 * 
 */
package utilities;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author YugandharReddyGorrep
 *
 */
import java.time.LocalDate;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
public class DateUtils
{
	public static String timeStamp ;
	public static boolean flag=false ;

	static Locale locale = Locale.getDefault();
	static TimeZone tz = TimeZone.getDefault();
	static Calendar cal = Calendar.getInstance(tz, locale);
	static Date d = new Date(System.currentTimeMillis());
	/**
	 * @description:Returns the current time stamp
	 * 
	 * 
	 * @return String
	 * @throws ParseException 
	 */
	public  String getCurrTimeStamp() throws ParseException
	{
		
		LocalDate.now();
		Locale locale = Locale.getDefault();
		TimeZone tz = TimeZone.getDefault();
		Calendar cal = Calendar.getInstance(tz, locale);
		Date d = new Date(System.currentTimeMillis());
		cal.setTime(d);
		int m = cal.get(Calendar.MONTH) + 1;
		int h = cal.get(Calendar.HOUR);
		int mm = cal.get(Calendar.MINUTE);
		int s = cal.get(Calendar.SECOND);
		 timeStamp = cal.get(Calendar.DAY_OF_MONTH) + "_" + m + "_" + cal.get(Calendar.YEAR) + "_" + h+"_"
				+ mm +"_" +s;
		System.out.println("date util time stamp in if+"+timeStamp);
		 SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");  
	    Date dateFormat=sdf.parse(timeStamp);
		return sdf.format(dateFormat);
		}
		
	
	public static String getCurrMonthInMM()
	{
		cal.setTime(d);
		int m = cal.get(Calendar.MONTH) + 1;
		String mm;
		if (m < 10)
			mm = "0" + m;
		else
			mm = Integer.toString(m);
		return mm;
	}
	public static String getCurrDateInDD()
	{
		int d = cal.get(Calendar.DATE);
		String mm;
		if (d < 10)
			mm = "0" + d;
		else
			mm = Integer.toString(d);
		return mm;
	}
	public static Integer getCurrYearInYYYY()
	{
		int y = cal.get(Calendar.YEAR);
		return y;
	}
	
	public static String dateChangeMMddyyyyToMMMddyyyy(String dateToChange) throws ParseException{  
		final String OLD_FORMAT = "MM/dd/yyyy";
		final String NEW_FORMAT = "MMM dd, yyyy";

		// August 12, 2010
		String oldDateString = dateToChange;
		String newDateString;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
		System.out.println(newDateString);
		return newDateString;
	}  
	
    public static String formatDate(String fromDateFormat, String toDateFormat, String fromDateString) throws ParseException
    {
           SimpleDateFormat sdf = new SimpleDateFormat(fromDateFormat);
           Date d = sdf.parse(fromDateString);
           sdf.applyPattern(toDateFormat);
           System.out.println("sdf.format(d)"+sdf.format(d));
           return sdf.format(d);            
    }

    public static String getCurrentDate() {
    	 Calendar cal = Calendar.getInstance();
	     // displaying date
	     Format f = new SimpleDateFormat("M/d/yyyy");
	     String strDate = f.format(new Date());
	     return strDate;
    }
    
    public static int getTimeDiff(String sd , String ed) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
		
	    Date firstDate = sdf.parse(sd);
	    Date secondDate = sdf.parse(ed);
	 
	    long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
	    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
	    System.out.println(diff);
	    
	    int i = (int) diff;
	    return i;
	}
	    
	public static String addDaysToDate(String sd,int days) throws ParseException
	{
		String dt="";
		SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
	    Calendar c = Calendar.getInstance();
	    c.setTime(sdf.parse(sd));
	    c.add(Calendar.DATE, days);  // number of days to add
	    dt = sdf.format(c.getTime());
	    System.out.println(dt);
	    return dt; 
	    
	 
	}
	public static String getPastDateFromToday(int days) throws ParseException
	{
		String dt="";
		String dateToday = getCurrentDate();
		int daysToMinus = (~(days - 1));
		SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
	    Calendar c = Calendar.getInstance();
	    c.setTime(sdf.parse(dateToday));
	    c.add(Calendar.DATE, daysToMinus);  // number of days to minus
	    dt = sdf.format(c.getTime());
	    System.out.println(dt);
	    return dt;
	}

}
