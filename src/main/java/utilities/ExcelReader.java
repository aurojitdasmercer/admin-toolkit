package utilities;

import static utilities.InitTests.dir_path;

import java.io.*;
import java.util.*;

import org.apache.poi.xssf.usermodel.*;

public class ExcelReader {

	/*public static void main(String a[])
	{
		try {
			String ss="6242,6246,4126,5700";
			System.out.println(ss.split(",").length);
			//System.out.println(getRowDataFromExcel("Verify_Add_ProductToOpportunity", "ProductData", 2));
		Map<String,String>map=getRowDataFromExcel("Verify_Add_ProductToOpportunity", "ProductData", 3);
			System.out.println("map"+map.get("Inv Segment"));
			//System.out.println(getRowDataFromExcel("Verify_Add_ProductToOpportunity", "ProductData", 4));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	public static ArrayList<String>ReadDriverSuiteExcel(String sheetName) {
		ArrayList<String>testcaseList= new ArrayList<String>();
		XSSFWorkbook Workbook_obj = null;
		XSSFSheet sheet_obj=null;
	

		
			try{
				ClassLoader loader = new ExcelReader().getClass().getClassLoader();
				 InputStream input = loader.getResourceAsStream("testdata/TestCases.xlsx");

				System.out.println(input.toString());
			
			System.out.println("Excel file read successfully...");
			Workbook_obj = new XSSFWorkbook(input);
			System.out.println("Before reading Excel sheet...."+sheetName);
			sheet_obj = Workbook_obj.getSheet(sheetName);
			Workbook_obj.close();
			System.out.println("Excel sheet read successfully..."+sheet_obj);
			
			int Row_count = sheet_obj.getLastRowNum();
			
			//System.out.print("Row count: "+Row_count+"\n");
			for (int i = 1;i<=Row_count;i++)
			{
				String Exec_indicator;
				//System.out.println("I value :"+i);
				XSSFRow row_obj = sheet_obj.getRow(i);
				XSSFCell cell_obj = row_obj.getCell(2);
				if(!(cell_obj==null))
				{
				 Exec_indicator = cell_obj.getStringCellValue();
				//System.out.print("Exec indicator: "+Exec_indicator+"\n");
				if((!Exec_indicator.isEmpty())&&!(Exec_indicator==""))
				{
				String Exec_ind = Exec_indicator.trim();
				//System.out.println("Trim String: "+Exec_ind+"\n");
				if (Exec_ind.equalsIgnoreCase("Y"))
				{
					XSSFCell cellobj1 = row_obj.getCell(1);
					String testcase = cellobj1.getStringCellValue();
					//System.out.println("testcase : "+testcase);
					testcaseList.add(testcase);
				}
				}
			}
			}
			}
		 catch (Exception e) {
			 e.printStackTrace();
			System.out.println("Exception "+e.getMessage());
		}
	
		
		return testcaseList;
	}
	
	public static Map<String,String> getRowDataFromExcel(String fileName,String sheetName,int rowNum) throws IOException {
		
		Properties props = new Properties();


		ClassLoader loader = new DataReaderMap().getClass().getClassLoader();
		 InputStream input = loader.getResourceAsStream("config/testdata.properties");
		props.load(input);
		if(props.getProperty("runWithCICD").contains("Y"))
			 dir_path = System.getProperty("user.dir");
		else
		 dir_path = props.getProperty("userdir");
		String excelPath=dir_path+"/src/main/resources/testdata/"+fileName+".xlsx";
		System.out.println("excelPath"+excelPath);
	
		System.out.println("in read excel sheet name --" + sheetName);
		FileInputStream file;
	
			file = new FileInputStream(new File(excelPath));
		
		// Get the workbook instance for XLS file
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheet(sheetName);
	      Map<String, String> datamap = new LinkedHashMap<String,String>();

		int lastRowNum = sheet.getLastRowNum() ;
	    int lastCellNum = sheet.getRow(rowNum).getLastCellNum();
	    for(int i=0;i<lastCellNum;i++)
	    {
	   String cellVal= sheet.getRow(rowNum).getCell(i).getStringCellValue();
	   if(cellVal==null ||cellVal.isEmpty()||cellVal=="")
	   {
		   
	   }
	   else
	   {
	   datamap.put(sheet.getRow(0).getCell(i).getStringCellValue(), cellVal);
	   }	 
		
	    }
		workbook.close();

	    return datamap;

	}
	
	
	public static int  getNoOfRows(String fileName,String sheetName) throws IOException {
		
		Properties props = new Properties();


		ClassLoader loader = new DataReaderMap().getClass().getClassLoader();
		 InputStream input = loader.getResourceAsStream("config/testdata.properties");
		props.load(input);
		if(props.getProperty("runWithCICD").contains("Y"))
			 dir_path = System.getProperty("user.dir");
		else
		 dir_path = props.getProperty("userdir");
		String excelPath=dir_path+"/src/main/resources/testdata/"+fileName+".xlsx";
		System.out.println("excelPath"+excelPath);
	
		System.out.println("in read excel sheet name --" + sheetName);
		FileInputStream file;
	
			file = new FileInputStream(new File(excelPath));
		
		// Get the workbook instance for XLS file
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheet(sheetName);

		workbook.close();
		int rows=sheet.getPhysicalNumberOfRows();
		System.out.println("rows"+rows);

		return rows;

	}
	
public static ArrayList<String> getColumnList(String fileName,String sheetName,int col,String dir_path) throws IOException {
		
		Properties props = new Properties();


		ClassLoader loader = new DataReaderMap().getClass().getClassLoader();
		 InputStream input = loader.getResourceAsStream("config/testdata.properties");
		props.load(input);
		if(props.getProperty("runWithCICD").contains("Y"))
			 dir_path = System.getProperty("user.dir");
		
		String excelPath=dir_path+"/src/main/resources/testdata/"+fileName+".xlsx";
		System.out.println("excelPath"+excelPath);
	
		System.out.println("in read excel sheet name --" + sheetName);
		FileInputStream file;
	
			file = new FileInputStream(new File(excelPath));
		
		// Get the workbook instance for XLS file
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		int rows=sheet.getPhysicalNumberOfRows();

	      Map<String, String> datamap = new LinkedHashMap<String,String>();


		int Row_count = sheet.getLastRowNum();
		ArrayList<String>testcaseList= new ArrayList<String>();

		//System.out.print("Row count: "+Row_count+"\n");
		for (int i = 1;i<=Row_count;i++)
		{
			//System.out.println("I value :"+i);
			XSSFRow row_obj = sheet.getRow(i);
			XSSFCell cell_obj = row_obj.getCell(2);
			String Exec_indicator = null;
			if(cell_obj!=null)
			 Exec_indicator = cell_obj.getStringCellValue();
			//System.out.print("Exec indicator: "+Exec_indicator+"\n");
			 String Exec_ind=null;
			 if(Exec_indicator!=null)
			 Exec_ind = Exec_indicator.trim();
			  if(Exec_ind==null)
			    	System.out.println("empty cell");
			//System.out.println("Trim String: "+Exec_ind+"\n");
			if (Exec_ind!=null && Exec_ind.equalsIgnoreCase("Y"))
			{
				XSSFCell cellobj1 = row_obj.getCell(1);
				if(cellobj1!=null)
				//String testcase = ;
				//System.out.println("testcase : "+testcase);
				testcaseList.add(cellobj1.getStringCellValue());
			}
		workbook.close();


	}
	    return testcaseList;

	
}   
}
