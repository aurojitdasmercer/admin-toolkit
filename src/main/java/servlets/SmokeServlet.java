package servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.ExcelReader;
import utilities.InitTests;
import utilities.XMLCreator;

/**
 * Servlet implementation class Regression_Servlet
 */

public class SmokeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String contextpath ;
	public static String contextroot;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException 
	{
		
		String env=request.getParameter("env");
		String browser=request.getParameter("browserName");		
		
		contextpath = this.getServletContext().getRealPath("/");
		
		contextroot = request.getContextPath();
		System.out.println("context path in reg servlet"+contextpath);
		System.out.println("context root in reg servlet"+contextroot);
System.setProperty("env", env);
System.setProperty("browserName", browser);
	
		// reading the user input    
		String testcase[]=request.getParameterValues("SmokeTestCases");
		List<String>testCases=new ArrayList<String>();
		testCases=Arrays.asList(testcase);
		//testCases=ExcelReader.getColumnList("VerifyMercerForce", "Regression", 2,contextpath);

		
		System.out.println("Running Testcases....");
		XMLCreator.xmlCreator(testCases,"smoke");
		System.out.println("***************I'm back in Servlet*********************");
		RequestDispatcher dispatcher = request.getRequestDispatcher("Jsp/index.jsp");
		dispatcher.forward( request, response );
		
	}  

}
