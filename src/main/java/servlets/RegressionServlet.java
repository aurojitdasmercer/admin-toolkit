package servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.InitTests;
import utilities.XMLCreator;

/**
 * Servlet implementation class SmokeServlet
 */
public class RegressionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String path ;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException 
	{
		

	
		// reading the user input    
		String testcase[]=request.getParameterValues("SmokeTestCases");
		String env=request.getParameter("env");
		String browser=request.getParameter("browserName");
		String opportunityName=request.getParameter("OpportunityName");
		String closeDate=request.getParameter("date");
		String Stage=request.getParameter("stage");
		String accountName=request.getParameter("Accountname");
		
		
System.setProperty("env", env);
System.setProperty("browserName", browser);
System.setProperty("OpportunityName", opportunityName);
System.setProperty("date", closeDate);
System.setProperty("stage",Stage);
System.setProperty("Accountname", accountName);

		ArrayList<String>testCases=new ArrayList<String>();
		for(int i=0;i<testcase.length;i++)
		{
			
			testCases.add(testcase[i]);
			//out.println("<li>"+testcase[i]+"</li>");
		}
		System.out.println("Running Testcases....");
		XMLCreator.xmlCreator(testCases,"smoke");
		System.out.println("***************I'm back in Servlet*********************");
		RequestDispatcher dispatcher = request.getRequestDispatcher("Jsp/index.jsp");
		dispatcher.forward( request, response );
		
	}  

}
