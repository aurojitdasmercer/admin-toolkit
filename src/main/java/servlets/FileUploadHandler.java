package servlets;

import static utilities.InitTests.dir_path;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileExistsException;

import utilities.DataReaderMap;
 
/**
 * Servlet to handle File upload request from Client
 * @author Yugandhar
 */
public class FileUploadHandler extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String fname;
	
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	Properties props=new Properties();
    	ClassLoader loader = new DataReaderMap().getClass().getClassLoader();
		 InputStream input = loader.getResourceAsStream("config/testdata.properties");
		props.load(input);
		if(props.getProperty("runWithCICD").contains("Y"))
			 dir_path = System.getProperty("user.dir");
		else
		 dir_path =this.getServletContext().getRealPath("/");
		String UPLOAD_DIRECTORY=dir_path+"/src/main/resources/testdata";
		File f = null;
		FileItem item1 = null; 
        //process only if its multipart content
        if(ServletFileUpload.isMultipartContent(request)){
            try {
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
               
                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                    	fname = new File(item.getName()).getName();
                    	f=new File(UPLOAD_DIRECTORY + File.separator + fname);
                   	 item1=item;

                        item.write(f);
                        
                    }
                }
                
            
               //File uploaded successfully
               request.setAttribute("message", "File Uploaded Successfully, You can run the tests");
           	System.out.println("File Upload done");
            }
             catch (FileExistsException ex) {
            	 try
            	 {
            	 if(f.delete())
            		 System.out.println("file delete success");
            	
                    	File ff=new File(UPLOAD_DIRECTORY + File.separator + fname);
                   	 System.out.println("fname"+fname+"ff"+ff);

                         item1.write(ff);
            	            
                 
             
                //File uploaded successfully
                request.setAttribute("message", "File Replaced Successfully, You can run the tests");
            	 }
            	catch(Exception e)
            	 {
                    request.setAttribute("message", "File Replace Failed due to " + ex);

            	 }
             }
             catch (Exception ex) {
            	System.out.println("File Upload Failed due to"+ex);
               request.setAttribute("message", "File Upload Failed due to " + ex);
            }          
          
        }else{
            request.setAttribute("message",
                                 "Sorry this Servlet only handles file upload request");
        }
     
        request.getRequestDispatcher("Jsp/index.jsp").forward(request, response);
      
    }
   
}
