package pages.AdminToolkit;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {

	
	
	@FindBy(xpath = "//td[@data-bind='text: level1']")
	public  List<WebElement> serviceContailerResults;
	
	@FindBy(xpath = "//select[@title='Select a Service Container item.']")
	public  WebElement serviceContainerDD;
	
	@FindBy(xpath = "//button[@data-bind='click: search']")
	public  WebElement  searchButton;
	
	@FindBy(xpath = "//a[@data-bind='click: reset']")
	public  List<WebElement>  resetButton;
	
	@FindBy(linkText = "[ Show Advanced Search ]")
	public WebElement  advancedSearchButton;
	
	@FindBy(xpath = "//label[text()='Advanced Search:']")
	public  WebElement  advancedSearchHeader;

	@FindBy(xpath = "//input[@data-bind='value: searchQuery']")
	public  WebElement searchTextBox;
	
	@FindBy(xpath = "(//button[text()='Search'])[2]")
	public  WebElement advSearchButton;
	
	@FindBy(xpath = "//div[@class='col-24']//p[1]")
	public  WebElement advSearchDesc;
	
	@FindBy(linkText = "[ Close Advanced Search ]")
	public  WebElement closeAdvSearch;
	
	
	
	WebDriver driver;

	public SearchPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	

	
		
	}
