package pages.AdminToolkit;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FileOperations {
	
	public ArrayList<String> HbSettingsName = new ArrayList<String>();
	public ArrayList<String> HbSettingsOptions = new ArrayList<String>();
	public ArrayList<String> portalSettingName = new ArrayList<String>();
	public ArrayList<String> portalSettingsOptions = new ArrayList<String>();
	public String expression;
	public String epOverideName;
	public String epOverideNameverification;
	public String companyId;
	public String overrideStatus;
	
	public  void readData() {
		
		try {
			String userdir = System.getProperty("user.dir");
			String fullFilePath = userdir+"\\src\\main\\resources\\testdata\\DataSheet.xlsx";
			System.out.println("Opening File: "+fullFilePath);
			FileInputStream file = new FileInputStream(new File(fullFilePath));
			XSSFWorkbook Workbook_obj = new XSSFWorkbook(file);
			XSSFSheet sheet_obj = Workbook_obj.getSheet("HBClientSettings");
			int maxrow = sheet_obj.getLastRowNum();
			System.out.println("Max Row: "+maxrow);
			DataFormatter df = new DataFormatter();
			
			for(int i=1;i<=maxrow;i++) {
				HbSettingsName.add(df.formatCellValue(sheet_obj.getRow(i).getCell(0)));
				HbSettingsOptions.add(df.formatCellValue(sheet_obj.getRow(i).getCell(1)));
				HbSettingsOptions.add(df.formatCellValue(sheet_obj.getRow(i).getCell(2)));
				
			}
			
			sheet_obj = Workbook_obj.getSheet("Portal");
			maxrow = sheet_obj.getLastRowNum();
			for(int i=1;i<=maxrow;i++) {
				portalSettingName.add(df.formatCellValue(sheet_obj.getRow(i).getCell(0)));
				portalSettingsOptions.add(df.formatCellValue(sheet_obj.getRow(i).getCell(1)));
			}
			
			sheet_obj = Workbook_obj.getSheet("EP");
			maxrow = sheet_obj.getLastRowNum();
			System.out.println("Max Row: "+maxrow);
			
			expression = df.formatCellValue(sheet_obj.getRow(1).getCell(1));
			epOverideName = df.formatCellValue(sheet_obj.getRow(2).getCell(1));
			epOverideNameverification = df.formatCellValue(sheet_obj.getRow(3).getCell(1));
			companyId = df.formatCellValue(sheet_obj.getRow(4).getCell(1));
			overrideStatus = df.formatCellValue(sheet_obj.getRow(5).getCell(1));
			Workbook_obj.close();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	

}
