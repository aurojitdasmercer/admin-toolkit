package pages.AdminToolkit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementContains;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
public class HomePage {

	@FindBy(xpath = "//h1[text()='Client List']")
	public  WebElement clientListHeader;
	
	@FindBy(id = "client-list-filter")
	public  WebElement filterSearchBox;
	
	@FindBy(id = "eval-points-filter")
	public  WebElement epSearchBox;
	
	@FindBy(xpath = "//td[@data-bind='text: OneCode']")
	public  WebElement clientCode;
	
	@FindBy(linkText = "Mercer Marketplace Enterprise")
	public  WebElement clientName;
	
	@FindBy(xpath = "//h2[text()='Web Configuration']")
	public  WebElement webConfiguration;
	
	@FindBy(xpath = "//h2[text()='Web Configuration']/following-sibling::p")
	public  WebElement WebConfigDesc;
	
	@FindBy(xpath = "//h1[@data-bind='text: clientName']")
	public  WebElement clientNameHeader;
	
	@FindBy(xpath = "//span[@data-bind='text: LOBs']")
	public  WebElement lineOFBusiness;
	
	@FindBy(xpath = "//span[text()='SMLMKT']")
	public  WebElement clientOneCOde;
	
	@FindBy(xpath = "//span[text()='Visibility Date:']/following-sibling::span")
	public  WebElement visibilityDate;
	
	@FindBy(linkText = "BPO / Carrier Descriptions")
	public  WebElement bpoCarrierDesctab;
	
	@FindBy(linkText = "Client Settings")
	public  WebElement clientSettingsTtab;
	
	

	@FindBy(xpath = "//td[@class='eval-point-description']")
	public  WebElement firstEpName;
	
	@FindBy(xpath = "//span[@data-bind='text: headerDescription']")
	public  WebElement epHeaderNAME;
	
	@FindBy(xpath = "//div[contains(@class,'blockUI blockMsg')]//img[1]")
	public  WebElement loadingIcon;
	
	@FindBy(linkText = "Current")
	public  WebElement currentFilter;
	
	@FindBy(xpath = "//strong[text()='DEFAULT RULE']")
	public  WebElement defaultRule;
	
	@FindBy(xpath = "//a[@title='Copy Rule']")
	public  WebElement copyRule;
	
	@FindBy(xpath = "//input[@title='Enter a rule name.']")
	public  WebElement ruleNameTextBox;
	
	@FindBy(xpath = "//input[@title='Enter a rule description.']")
	public  WebElement ruleDesctextBox;
	
	@FindBy(css = "textarea[class='ace_text-input']")
	public  WebElement expressionTextBox;
	
	@FindBy(id = "Copy")
	public  WebElement copyTextBox;
	
	@FindBy(xpath = "//select[@id='Select2']")
	public  WebElement link1Target;
	
	@FindBy(xpath = "//select[@id='Select3']")
	public  WebElement link1Style;
	
	@FindBy(xpath = "(//select[@id='Select2'])[2]")
	public  WebElement link2Target;
	
	@FindBy(xpath = "(//select[@id='Select3'])[2]")
	public  WebElement link2Style;
	
	@FindBy(xpath = "//button[@title='Save']")
	public  WebElement saveButton;
	
	@FindBy(id = "Button3")
	public  WebElement validateSyntax;
	
	@FindBy(xpath = "//p[text()='The expression you have written is valid.']")
	public  WebElement vadlidExpMsg;
	
	@FindBy(xpath = "//p[@data-bind='html: confirmation']")
	public  WebElement saveConfirmationMsg;
	
	@FindBy(xpath = "(//strong[text()='AUTOMATION RULE'])")
	public  WebElement automationRule;
	
	@FindBy(xpath = "//a[@title='Delete Rule']")
	public  WebElement deleteRule;
	
	@FindBy(xpath = "//button[text()='Delete']")
	public  WebElement deleteButton;
	
	@FindBy(xpath = "//p[@data-bind='html: confirmation']")
	public  WebElement deleteConfirmationMsg;
	

	@FindBy(xpath = "//a[@title='Edit Rule']")
	public  WebElement editButton;
	
	@FindBy(linkText = "Home")
	public  WebElement homeTab;
	
	@FindBy(xpath = "//select[@title='Select a choice.']")
	public  WebElement benifitIDDD;
	
	@FindBy(xpath = "//span[text()='{Update}']")
	public  WebElement updateImage;
	
	@FindBy(id = "upload-media-library-file")
	public  WebElement browseImage;
	
	@FindBy(xpath = "//input[@name='keywords']")
	public  WebElement imageKeyword;
	
	@FindBy(xpath = "//input[@name='description']")
	public  WebElement imageDesc;
	
	@FindBy(xpath = "//button[@title='Save and Select']")
	public  WebElement saveImageButton;
	
	@FindBy(xpath = "//span[text()='image1.jpg']")
	public  WebElement imageName;
	
	
	@FindBy(id = "Value")
	public  WebElement valueTextBox;
	
	@FindBy(xpath = "//span[text()='View HTML']")
	public  WebElement viewHtmlButton;
	
	@FindBy(xpath = "//textarea[@class='k-editor-textarea k-input']")
	public  WebElement htmlContent;
	
	@FindBy(xpath = "//button[text()='Update']")
	public  WebElement updateButton;
	
	
	@FindBy(id = "Header")
	public  WebElement headertextBox;
	
	@FindBy(xpath = "//input[@class='col-19']")
	public List<WebElement> inputBox;
	
	@FindBy(xpath = "//textarea[@rows='4']")
	public  WebElement bodyTextBox;
	
	@FindBy(xpath = "//div[@id='Title']")
	public  WebElement titleTextBox;
	@FindBy(xpath = "(//div[@id='Title'])[2]")
	public  WebElement title2TextBox;
	
	@FindBy(xpath = "//button[text()='Update']")
	public  WebElement body2TextBox;
	
	@FindBy(linkText = "Best Match")
	public  WebElement bestMatchTab;
	
	@FindBy(xpath = "//a[@title='Clone Evaluation Point']")
	public  WebElement cloneEp;
	
	@FindBy(id = "Select1")
	public  WebElement cmpnyID;
	
	@FindBy(xpath = "//option[@value='*ALL*']")
	public  WebElement eligibilityGrpId;
	

	@FindBy(xpath = "//button[@title='Submit']")
	public  WebElement submitButton;
	
	@FindBy(xpath = "//p[text()='Override for Evaluation Point generated successfully.']")
	public  WebElement overrideSuccessMsg;
	
	@FindBy(xpath = "//span[text()='Generate Evalution Point Override']")
	public  WebElement genereateOverrideHeader;
	
	@FindBy(xpath = "//p[text()='Please select the override options for this Evaluation Point']")
	public  WebElement genereateOverrideDesc;
	
	@FindBy(xpath = "//a[@title='Generate Evaluation Point Override']")
	public  WebElement genereateOverride;
	
	@FindBy(xpath = "//span[text()='Authoring Override status:']/following-sibling::span")
	public  WebElement authoringOverrideStatus;
	
	@FindBy(xpath = "//span[@data-bind='text: overrideDeliveryRecordStatus']")
	public  WebElement deliveryOverrideStatus;
	
	
	@FindBy(xpath = "//input[@title='Enter a title.']")
	public  WebElement ruleLabel;
	
	@FindBy(xpath = "//a[@title='Delete Override Record From Authoring']")
	public  WebElement delOverrideAuth;
	
	@FindBy(xpath = "//a[@title='Delete Override Record From Delivery']")
	public  WebElement delOverrideDel;
	

	@FindBy(xpath = "//p[text()='Override for Evaluation Point deleted successfully (Authoring).']")
	public  WebElement delOverrideAuthSuccess;
	
	@FindBy(xpath = "//p[text()='Override for Evaluation Point deleted successfully (Authoring).']")
	public  WebElement delOverrideDelSuccess;
	
	@FindBy(xpath = "//h6[text()='Error']")
	public  WebElement delOverrideErrorMsg;
	
	@FindBy(xpath = "//h6[text()='Confirmation']")
	public  WebElement delOverrideSuccessMsg;

	
	WebDriver driver;
	public String date;

	public HomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
		SimpleDateFormat df= new SimpleDateFormat("M/d/YYYY");
		date = df.format(new Date());
		
	}
	
	
	public void waitForPageload() {
		
		System.out.println("Waiting For Page To Load.... :(");
		waitForElementToDisappear(By.xpath("(//img[@src='assets/images/loading_animation_bar.gif'])[1]"));
		waitForElementToDisappear(By.xpath("(//img[@src='assets/images/loading_animation_bar.gif'])[2]"));
		//(//img[@src='assets/images/loading_animation_bar.gif'])[2]
		System.out.println("Page Loaded :)");
	}
	
	public void openDefaultRule() throws InterruptedException {
		waitForElementToDisplay(firstEpName);
		clickElement(firstEpName);
		waitForPageload();	
		try {
			Thread.sleep(2000);
			//waitForElementToDisplay(currentFilter);	
			clickElement(currentFilter);
		} catch (Exception e) {
			
		}
		waitForElementToDisplay(defaultRule);
		clickElement(defaultRule);
		
	}
	
	public void copyRule(String exp) throws InterruptedException {
		Thread.sleep(1000);
		clickElement(copyRule);
		waitForElementToDisplay(ruleNameTextBox);				
		setInput(ruleNameTextBox, "Automation Rule");
		setInput(ruleDesctextBox, "Automation Rule");
		
		expressionTextBox.sendKeys(exp);
		clickElement(validateSyntax);
		/*try {
			copyTextBox.sendKeys("Automation Rule Copy");
		} catch (Exception e) {
			
		}*/
		
	}
	
	public void deleteRule() throws InterruptedException {
		Thread.sleep(10000);
		clickElement(automationRule);
		scrollToElement(driver, deleteRule);
		clickElement(deleteRule);
		waitForElementToDisplay(deleteButton);
		clickElement(deleteButton);
		waitForElementToDisplay(deleteConfirmationMsg);
	}
	
	public void editRule() throws InterruptedException {
		waitForElementToDisplay(editButton);
		Thread.sleep(1000);
		clickElement(editButton);
		/*waitForElementToDisplay(copyTextBox);
		try {
			copyTextBox.sendKeys("Automation Rule Copy");
		} catch (Exception e) {
			
		}*/
		
	}
	
	public void updateValues() {
		try {
			headertextBox.sendKeys("Automation");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
		try {
			bodyTextBox.sendKeys("Automation");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			titleTextBox.sendKeys("Automation");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			body2TextBox.sendKeys("Automation");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
		}
		try {
			title2TextBox.sendKeys("Automation");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cloneEp(String cmpnyid) throws InterruptedException {
		
		clickElement(cloneEp);
		waitForElementToDisplay(cmpnyID);
		Select select = new Select(cmpnyID);
		select.selectByVisibleText(cmpnyid);
		waitForElementToDisplay(eligibilityGrpId);
		clickElement(eligibilityGrpId);
		
		
	}
		
	}
