package pages.AdminToolkit;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BestMatchPage {

	
	
	@FindBy(xpath = "//h2[text()='Web Configuration - Best Match']")
	public  WebElement bestMatchHeader;
	
	@FindBy(xpath = "//p[@align='left']")
	public  WebElement bestMatchDesc;
	
	@FindBy(xpath = "//a[@title='Cancel']")
	public WebElement  cancelButton;
	
	@FindBy(linkText= "Save")
	public  WebElement  saveButton;
	
	@FindBy(xpath = "//label[@class='col-14']")
	public  List<WebElement>  portalSettingsName;
	
	@FindBy(xpath = "//input[@class='col-2 js-mul-numeric-mask']")
	public  List<WebElement>  portalSettingstextbox;

	@FindBy(linkText = "Portal")
	public  WebElement portalTab;
	
	WebDriver driver;

	public BestMatchPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	

	
		
	}
