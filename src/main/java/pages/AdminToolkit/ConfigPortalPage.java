package pages.AdminToolkit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;

public class ConfigPortalPage {
	WebDriver driver;
	
	public ConfigPortalPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="username")	
	public  WebElement userName;
	
	@FindBy(xpath="//img[@alt='Client logo']")	
	public  WebElement homeIconMM365;
	
	@FindBy(id="password")	
	public  WebElement password;
	
	@FindBy(css="button[class='mas-btn mas-btn-primary']")	
	public  WebElement continueButton;
	
	@FindBy(css="a[class='site-logo-wrapper']")	
	public  WebElement homeIcon;
	
	@FindBy(xpath="//a[@data-e2e-name='administration tab']")	
	public  WebElement adminTab;
	
	@FindBy(xpath="(//a[@ng-disabled='!customizationAllowed()'])[3]")	
	public  WebElement promoteEvalPoint;
	
	@FindBy(xpath="//h1[text()='Promotion Evaluation Engine Overrides']")	
	public  WebElement promoteEvalPointHeader;
	
	@FindBy(tagName="textarea")	
	public  WebElement promoteEvalPointHInput;
	
	@FindBy(css="a[ng-click='saveButton()']")	
	public  WebElement promoteButton;
	
	@FindBy(xpath="//div[text()[normalize-space()='All Employee Website Eval Engine Overrides for SpaceX have been promoted to Delivery successfully!']]")	
	public  WebElement promoteSucessMsg;


	@FindBy(xpath="//h3[text()='Just For You']")	
	public  WebElement JustForYouHeader;
	
	public void login(String usrname, String pass) throws InterruptedException {
		setInput(userName, usrname);
		setInput(password, pass);
		System.out.println("Clicking continue button...");
		clickElement(continueButton);
		System.out.println("Waiting for page to load...");
		waitForElementToDisappear(By.cssSelector("div[class='mas-loading-indicator']"));		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		System.out.println("Page  loaded...");
	}
	
	public void waitForLoadingPage() {
		
		waitForElementToDisplay(By.cssSelector("div[class='blocking-overlay']"));
		waitForElementToDisappear(By.cssSelector("div[class='blocking-overlay']"));
		//waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("page-container broker-area"));
		System.out.println("Page  loaded...");
	}
}
