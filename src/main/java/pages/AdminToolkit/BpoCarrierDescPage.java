package pages.AdminToolkit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

public class BpoCarrierDescPage {

	
	
	@FindBy(xpath = "//h2[text()='Web Configuration - BPO / Carrier Descriptions (']")
	public  WebElement webConfigurationBPO;
	
	@FindBy(xpath = "//h2[contains(@class,'mul-hdr-rule mul-hdr-icons')]/following-sibling::p[1]")
	public  WebElement WebConfigBPODesc;
	
	@FindBy(xpath = "//h1[@data-bind='text: clientName']")
	public  WebElement clientNameHeader;
	
	@FindBy(xpath = "//span[text()='Carrier Descriptions']")
	public  WebElement carrierDescriptions;
	
	@FindBy(xpath = "//span[text()='Benefit Descriptions']")
	public  WebElement benifitDescriptions;
	
	@FindBy(xpath = "//span[text()='Plan Descriptions']")
	public  WebElement PlanDescriptions;
	
	@FindBy(xpath = "//span[text()='Option Descriptions']")
	public  WebElement optionsDescription;
	
	@FindBy(xpath = "//h2[text()='Web Configuration - Carrier Descriptions']")
	public  WebElement webConfigurationCarrierDesc;
	
	@FindBy(xpath = "//h2[text()='Web Configuration - Benefit Descriptions']")
	public  WebElement webConfigurationBenifitsDesc;
	
	@FindBy(xpath = "//h2[text()='Web Configuration - Plan Descriptions']")
	public  WebElement webConfigurationPlanDesc;

	@FindBy(xpath = "//h2[text()='Web Configuration - Option Descriptions']")
	public  WebElement webConfigurationoptionDesc;
	
	@FindBy(xpath = "//input[@placeholder='Filter']")
	public  WebElement carrierFilter;
	
	@FindBy(xpath = "//a[@title='Edit']")
	public  WebElement edit1StCarrier;
	
	@FindBy(id = "short-description")
	public  WebElement carrierShortDesc;
	
	@FindBy(xpath = "//input[@title='Enter a option description.']")
	public  WebElement optionShortDesc;
	
	@FindBy(xpath = "//input[@title='Enter a plan description.']")
	public  WebElement planShortDesc;
	
	@FindBy(xpath = "//a[@title='Cancel']")
	public  WebElement cancelCarrier;
	
	@FindBy(linkText = "Save")
	public  WebElement saveCarrier;
	
	@FindBy(linkText = "Save and Close")
	public  WebElement saveAndContinue;
	
	
	
	@FindBy(xpath = "//p[text()='You have successfully updated ']")
	public  WebElement saveCarrierSuccesmsg;
	
	String carrierDescvalue;

	
	WebDriver driver;

	public BpoCarrierDescPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void searchAndEditCarrier(String word2Search) throws InterruptedException {
		
		waitForElementToDisplay(carrierFilter);
		setInput(carrierFilter, word2Search);
		waitForElementToDisplay(edit1StCarrier);
		clickElement(edit1StCarrier);
		carrierDescvalue = carrierShortDesc.getAttribute("value");
		carrierShortDesc.sendKeys(" Automation");
		clickElement(saveCarrier);
		
	}
	
public void searchAndEditCarrierPlan(String word2Search) throws InterruptedException {
		
		waitForElementToDisplay(carrierFilter);
		setInput(carrierFilter, word2Search);
		waitForElementToDisplay(edit1StCarrier);
		clickElement(edit1StCarrier);
		carrierDescvalue = planShortDesc.getAttribute("value");
		planShortDesc.sendKeys(" Automation");
		clickElement(saveCarrier);
		
	}
	
public void searchAndEditOption(String word2Search) throws InterruptedException {
		
		waitForElementToDisplay(carrierFilter);
		setInput(carrierFilter, word2Search);
		waitForElementToDisplay(edit1StCarrier);
		clickElement(edit1StCarrier);
		carrierDescvalue = optionShortDesc.getAttribute("value");
		optionShortDesc.sendKeys(" Automation");
		clickElement(saveCarrier);
		
	}
	
public void EditCarrierRename() throws InterruptedException {
		
		
		waitForElementToDisplay(edit1StCarrier);
		clickElement(edit1StCarrier);
		setInput(carrierShortDesc, carrierDescvalue);		
		clickElement(saveCarrier);
		
	}

public void EditCarrierRenameOption() throws InterruptedException {
	
	
	waitForElementToDisplay(edit1StCarrier);
	clickElement(edit1StCarrier);
	setInput(optionShortDesc, carrierDescvalue);		
	clickElement(saveCarrier);
	
}
public void EditCarrierRenamePaln() throws InterruptedException {
	
	
	waitForElementToDisplay(edit1StCarrier);
	clickElement(edit1StCarrier);
	setInput(planShortDesc, carrierDescvalue);		
	clickElement(saveCarrier);
	
}
	

	
		
	}
