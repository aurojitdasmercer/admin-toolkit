package pages.AdminToolkit;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientSettingsPage {

	
	
	@FindBy(xpath = "//span[text()='Web Configuration - Client Settings']")
	public  WebElement ClientSettingsHeader;
	
	@FindBy(xpath = "//h3[text()='HB Settings']")
	public  WebElement hbSettingsHeader;
	
	@FindBy(className = "col-14")
	public  List<WebElement>  hbSettingsName;
	
	@FindBy(xpath = "//span[@data-bind='text: text']")
	public  List<WebElement>  hbSettingsOptions;
	
	@FindBy(xpath = "//label[@class='col-14']")
	public  List<WebElement>  portalSettingsName;
	
	@FindBy(xpath = "//input[@class='col-2 js-mul-numeric-mask']")
	public  List<WebElement>  portalSettingstextbox;

	@FindBy(linkText = "Portal")
	public  WebElement portalTab;
	
	@FindBy(linkText = "Visibility Date Client Configuration")
	public  WebElement visibilityDateTab;
	
	@FindBy(xpath = "//input[@type='radio']")
	public  WebElement enabledRadioButton;
	
	@FindBy(xpath = "//h3[text()='Visibility Date']")
	public  WebElement visibilityDateHeader;
	
	@FindBy(xpath = "//a[@title='Cancel']")
	public  WebElement visibilityDateCancel;
	
	@FindBy(xpath = "//button[@title='Save']")
	public  WebElement visibilityDateSave;
	
	@FindBy(xpath = "(//a[contains(text(),'24')])[3]")
	public  WebElement visibilityDateInput;
	
	@FindBy(className = "ui-datepicker-trigger")
	public  WebElement datePicker;
	
	@FindBy(xpath = "//button[@title='Discard and continue']")
	public  WebElement discardButton;
	
	@FindBy(xpath = "//p[text()='Your changes have not been saved. Are you sure you want to cancel?']")
	public  WebElement discardMsg;
	
	@FindBy(linkText = "HB Client Settings")
	public  WebElement hbClientSettingsTab;
	
	WebDriver driver;

	public ClientSettingsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	

	
		
	}
