package smoke.adminToolkit.tests;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.AdminToolkit.FileOperations;
import pages.AdminToolkit.HomePage;
import utilities.InitTests;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;


public class TC2 extends InitTests {
	TC2  verifySiteIsUp;
	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	
	public TC2(String appName) {
		super(appName);
	}
	
	@Test(enabled=true, priority= 1, retryAnalyzer = RetryAnalyzer.class)
	public void verifyQM() throws Exception	{		
			
		try {
			verifySiteIsUp=	new TC2("adminToolkit");	
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Client and Home");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			
			HomePage home = new HomePage(driver);
			FileOperations file = new FileOperations();
			file.readData();
			
			waitForElementToDisplay(home.clientListHeader);
			verifyElementContains(home.clientListHeader, "Client List", " , Client List Header Matched", test);			
			setInput(home.filterSearchBox, "smlmkt");
			waitForElementToDisplay(home.clientName);
			verifyElementContains(home.clientName, "Mercer Marketplace Enterprise", " , Client Name Header Matched", test);
			verifyElementContains(home.clientCode, "SMLMKT", " , Client OneCode Name Header Matched", test);
			clickElement(home.clientName);
			
			waitForElementToDisplay(home.webConfiguration);
			verifyElementContains(home.webConfiguration, "Web Configuration", " , Web Configuration Header Matched", test);
			verifyElementContains(home.WebConfigDesc, "The following are Evaluations Points for SMLMKT.", " ,  Web Configuration Desc Matched", test);
			verifyElementContains(home.clientNameHeader, "Mercer Marketplace Enterprise", " ,  Mercer Marketplace Enterprise header Matched", test);
			verifyElementContains(home.lineOFBusiness, "HB,PORTAL", " ,  Line of Business Matched", test);
			verifyElementContains(home.clientOneCOde, "SMLMKT", " ,  Client code Matched", test);
			verifyElementContains(home.visibilityDate, home.date, " , Visibility Date Matched", test);
			
			home.waitForPageload();
			
			// 1st EP
			test = reports.createTest("Verify EP Medical What To Do Here (Life Event)");
			setInput(home.epSearchBox, "Medical What To Do Here (Life Event)");		
			home.openDefaultRule();
			waitForElementToDisplay(home.epHeaderNAME);
			verifyElementContains(home.epHeaderNAME, "Medical What To Do Here (Life Event)", ", EP Header Matched", test);
			home.copyRule(file.expression);
			waitForElementToDisplay(home.vadlidExpMsg);
			verifyElementContains(home.vadlidExpMsg, "The expression you have written is valid.", ", Expression validated", test);
			try {
			home.copyTextBox.sendKeys("Automation Rule Copy");
		} catch (Exception e) {
			
		}
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			
			home.editRule();
			waitForElementToDisplay(home.copyTextBox);
			try {
				home.copyTextBox.sendKeys("Automation Rule Copy");
			} catch (Exception e) {
				
			}
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			waitForElementToDisplay(home.loadingIcon);
			
			home.deleteRule();
			System.out.println(home.deleteConfirmationMsg.getText());
			verifyElementContains(home.deleteConfirmationMsg, "The rule 'AUTOMATION RULE' was successfully deleted.", " Rule Deleted", test);
			
			//2ndEP
			test = reports.createTest("Verify EP Image for Pet Insurance");
			waitForElementToDisplay(home.homeTab);
			clickElement(home.homeTab);
			setInput(home.epSearchBox, "Image for Pet Insurance");		
			home.openDefaultRule();
			waitForElementToDisplay(home.epHeaderNAME);
			verifyElementContains(home.epHeaderNAME, "Image for Pet Insurance", ", EP Header Matched", test);
			home.copyRule(file.expression);
			waitForElementToDisplay(home.vadlidExpMsg);
			verifyElementContains(home.vadlidExpMsg, "The expression you have written is valid.", ", Expression validated", test);
			try {
				home.copyTextBox.sendKeys("Automation Rule Copy");
			} catch (Exception e) {
				
			}
			Select select = new Select(home.benifitIDDD);
			select.selectByVisibleText("PET");
			String userdir = System.getProperty("user.dir");
			String fullFilePath = userdir+"\\src\\main\\resources\\testdata\\image1.jpg";
			clickElement(home.updateImage);
			waitForElementToDisplay(home.imageDesc);
			Thread.sleep(2000);
			home.browseImage.sendKeys(fullFilePath);
			setInput(home.imageKeyword, "Automation");
			setInput(home.imageDesc, "Test");			
			clickElement(home.saveImageButton);
			waitForElementToDisplay(home.imageName);
			verifyElementContains(home.imageName,  "image1.jpg", ", Name Matched", test);
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			
			home.editRule();
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			waitForElementToDisappear(By.xpath("//div[contains(@class,'blockUI blockMsg')]//img[1]"));
			home.deleteRule();
			System.out.println(home.deleteConfirmationMsg.getText());
			verifyElementContains(home.deleteConfirmationMsg, "The rule 'AUTOMATION RULE' was successfully deleted.", " Rule Deleted", test);
			
			//3rd EP
			test = reports.createTest("Verify EP Who's Covered Dependent Disclaimer");
			String htmlValue = "Your dependent(s) must meet the eligibility criteria as outlined in the <a href=\"/media//Default/Forms/TestForm\" target=\"_blank\">Dependent Eligibility Document</a>&nbsp;. Documentation may be requested at any time to show your proof of eligibility for a covered dependent. If it is determined that your dependent does not meet the plan eligibility rules, you may be subject to penalties that may include repayment of premium, repayment of claims and disciplinary action.";
			
			waitForElementToDisplay(home.homeTab);
			clickElement(home.homeTab);
			setInput(home.epSearchBox, "Who's Covered Dependent Disclaimer");		
			home.openDefaultRule();
			waitForElementToDisplay(home.epHeaderNAME);
			verifyElementContains(home.epHeaderNAME, "Who's Covered Dependent Disclaimer", ", EP Header Matched", test);
			home.copyRule(file.expression);
			waitForElementToDisplay(home.vadlidExpMsg);
			verifyElementContains(home.vadlidExpMsg, "The expression you have written is valid.", ", Expression validated", test);
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			
			home.editRule();
			
			clickElement(home.valueTextBox);
			waitForElementToDisplay(home.viewHtmlButton);
			clickElement(home.viewHtmlButton);
			waitForElementToDisplay(home.htmlContent);
			System.out.println(home.htmlContent.getAttribute("value"));
			verifyElementContains(home.htmlContent, htmlValue, "Html value matched", test);
			clickElement(home.updateButton);
			
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			waitForElementToDisappear(By.xpath("//div[contains(@class,'blockUI blockMsg')]//img[1]"));
			
			home.deleteRule();
			System.out.println(home.deleteConfirmationMsg.getText());
			verifyElementContains(home.deleteConfirmationMsg, "The rule 'AUTOMATION RULE' was successfully deleted.", " Rule Deleted", test);
			
			
			//4th EP
			test = reports.createTest("Verify EP Dependent Benefit Eligibility Javascript");
			waitForElementToDisplay(home.homeTab);
			clickElement(home.homeTab);
			setInput(home.epSearchBox, "Dependent Benefit Eligibility Javascript");		
			home.openDefaultRule();
			waitForElementToDisplay(home.epHeaderNAME);
			verifyElementContains(home.epHeaderNAME, "Dependent Benefit Eligibility Javascript", ", EP Header Matched", test);
			//home.copyRule(file.expression);
			
			
			
			Thread.sleep(1000);			
			clickElement(home.copyRule);
			waitForElementToDisplay(home.ruleNameTextBox);				
			setInput(home.ruleNameTextBox, "Automation Rule");
			setInput(home.ruleDesctextBox, "Automation Rule");
			
			home.expressionTextBox.sendKeys("true");
			clickElement(home.validateSyntax);
			
			
			waitForElementToDisplay(home.vadlidExpMsg);
			verifyElementContains(home.vadlidExpMsg, "The expression you have written is valid.", ", Expression validated", test);
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			
			home.editRule();	
			
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			waitForElementToDisappear(By.xpath("//div[contains(@class,'blockUI blockMsg')]//img[1]"));
			
			home.deleteRule();
			System.out.println(home.deleteConfirmationMsg.getText());
			verifyElementContains(home.deleteConfirmationMsg, "The rule 'AUTOMATION RULE' was successfully deleted.", " Rule Deleted", test);
			
			//5th EP
			test = reports.createTest("Verify EP Get Started Help Center");
			waitForElementToDisplay(home.homeTab);
			clickElement(home.homeTab);
			setInput(home.epSearchBox, "Get Started Help Center");		
			home.openDefaultRule();
			waitForElementToDisplay(home.epHeaderNAME);
			verifyElementContains(home.epHeaderNAME, "Get Started Help Center", ", EP Header Matched", test);
			home.copyRule(file.expression);
			waitForElementToDisplay(home.vadlidExpMsg);
			verifyElementContains(home.vadlidExpMsg, "The expression you have written is valid.", ", Expression validated", test);
			home.updateValues();
			
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			
			home.editRule();	
			
			
			clickElement(home.saveButton);
			waitForElementToDisplay(home.saveConfirmationMsg);
			verifyElementContains(home.saveConfirmationMsg, "You have successfully edited the following rule: AUTOMATION RULE and published the Evaluation Point.", " Save Sucess", test);
			System.out.println(home.saveConfirmationMsg.getText());
			waitForElementToDisappear(By.xpath("//div[contains(@class,'blockUI blockMsg')]//img[1]"));
			
			home.deleteRule();
			System.out.println(home.deleteConfirmationMsg.getText());
			verifyElementContains(home.deleteConfirmationMsg, "The rule 'AUTOMATION RULE' was successfully deleted.", " Rule Deleted", test);
			
			
			
			
			
		}
		
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
			driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
			driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.quit();

		}
	}
}
	
	